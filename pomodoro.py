# this is timely reminder script.
# it will ask you to give two times.
# first the working time
# second the break time
# basically working on pomodoro technique.

import os
from time import sleep as sleep

pwd = os.path.dirname(os.path.abspath(__file__))
os.chdir(pwd)

work_time = int(raw_input("give the working time. Default time is 25 minutes : ") or "25")
chill_time = int(raw_input("give the chill out time. Default time is 3 minutes : ") or "3")
total_time = int(raw_input("give the total working time. Default is 3 hours : ") or "3")*60

loop_count = (total_time/(work_time+chill_time+1))+1



def notificationsystem(work_time, chill_time):
    print "stop procrastinating and start working"
    os.system('mpg321 start.mp3 &')
    sleep(work_time*60)
    print ("take a break for %s minutes" % chill_time)
    os.system('mpg321 stop.mp3 &')
    sleep(chill_time*60)


while loop_count > 0:
    notificationsystem(work_time, chill_time)
    loop_count -= 1

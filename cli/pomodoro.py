#!/usr/bin/env python

# working with CLI

import click
import os
from time import sleep as sleep


@click.command()
@click.option('-w', default=25, help='Specify the working time in minutes')
@click.option('-r', default=3, help='Specify the rest time in minutes')
@click.option('-t', default=3, help='Specify the total time in hours')
def pom(w, r, t):
    """@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
    Simple pomodoro for your daily procrastination buster.
    Use this program to set the basic work + rest time cycle.
    Without any arguments it takes 25 min as your work time, 3 minutes as your wait time and runs for 3 hours
    If you want any difference specify using -w for work time -r for rest time and -t for total time.
    For help try --help."""
    work_time = w
    chill_time = r
    total_time = t * 60
    loop_count = (total_time / (work_time + chill_time + 1)) + 1

    def notificationsystem(work_time, chill_time):
        print "stop procrastinating and start working"
        os.system('mpg321 asset/start.mp3 &')
        sleep(work_time * 60)
        print ("take a break for %s minutes" % chill_time)
        os.system('mpg321 asset/stop.mp3 &')
        sleep(chill_time * 60)

    while loop_count > 0:
        notificationsystem(work_time, chill_time)
        loop_count -= 1


if __name__ == '__main__':
    pom()
